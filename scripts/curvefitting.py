import argparse
import json
import matplotlib.pyplot as plt


def arg():
	parser = argparse.ArgumentParser("Tool for curve fitting")
	parser.add_argument("-i", "--input_1", help = "json file from fastp", type = str, required = True)
	parser.add_argument("-I", "--input_2", help = "json file from known good sequencing", type = str, required = True)
	return parser.parse_args()


def read_json():
	args = arg()
	with open(args.input_1, "r") as f:
		data_1 = json.load(f)
	with open(args.input_2, "r") as f:
		data_2 = json.load(f)
	return (data_1, data_2)


#This part is only for trainings purpose
d1, d2 = read_json()
quality_curves = d1["read1_before_filtering"]["quality_curves"]
quality_A = quality_curves["A"]
quality_T = quality_curves["T"]
quality_G = quality_curves["G"]
quality_C = quality_curves["C"]
quality_mean = quality_curves["mean"]
quality_curves_2 = d2["read1_before_filtering"]["quality_curves"]
quality_mean_2 = quality_curves["mean"]

plt.plot(quality_mean,"r", quality_mean_2, "blue", linewidth=1)
plt.grid()
plt.xscale("log")
plt.legend(["1","2"], markerscale=1)

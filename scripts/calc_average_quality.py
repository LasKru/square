#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 16:58:12 2020

@author: lasse
"""
import argparse
import os
import json
import numpy as np

def arg():
    parser = argparse.ArgumentParser("Calculate Average Quality Scores")
    parser.add_argument("-d", "--directory", help = "path to directory with known good data from fastp (.json)")
    parser.add_argument("-o", "--output", help = "Outputfile")
    parser.add_argument("-p", "--paired", help = "paired end reads", default = False, action = "store_true")
    return(parser.parse_args())

args = arg()

def read_json(path):
    with open(args.directory + "/" + path, "r") as f:
        data = json.load(f)
    return(data)

read1_mean_values = []
read1_A_values = []
read1_T_values = []
read1_C_values = []
read1_G_values = []

read2_mean_values = []
read2_A_values = []
read2_T_values = []
read2_C_values = []
read2_G_values = []

for filename in os.listdir(args.directory):
    data = read_json(filename)
    
    read1_mean_values.append(data["read1_before_filtering"]["quality_curves"]["mean"])
    read1_A_values.append(data["read1_before_filtering"]["quality_curves"]["A"])
    read1_T_values.append(data["read1_before_filtering"]["quality_curves"]["T"])
    read1_C_values.append(data["read1_before_filtering"]["quality_curves"]["C"])
    read1_G_values.append(data["read1_before_filtering"]["quality_curves"]["G"])
    if args.paired:
        read2_mean_values.append(data["read2_before_filtering"]["quality_curves"]["mean"])
        read2_A_values.append(data["read2_before_filtering"]["quality_curves"]["A"])
        read2_T_values.append(data["read2_before_filtering"]["quality_curves"]["T"])
        read2_C_values.append(data["read2_before_filtering"]["quality_curves"]["C"])
        read2_G_values.append(data["read2_before_filtering"]["quality_curves"]["G"])


quality = {"read1": {},"read2": {}}

quality["read1"]["mean"] = list(np.mean(read1_mean_values, axis = 0))
quality["read1"]["A"] = list(np.mean(read1_A_values, axis = 0))
quality["read1"]["T"] = list(np.mean(read1_T_values, axis = 0))
quality["read1"]["C"] = list(np.mean(read1_C_values, axis = 0))
quality["read1"]["G"] = list(np.mean(read1_G_values, axis = 0))
if args.paired:
    quality["read2"]["mean"] = list(np.mean(read2_mean_values, axis = 0))
    quality["read2"]["A"] = list(np.mean(read2_A_values, axis = 0))
    quality["read2"]["T"] = list(np.mean(read2_T_values, axis = 0))
    quality["read2"]["G"] = list(np.mean(read2_C_values, axis = 0))
    quality["read2"]["C"] = list(np.mean(read2_G_values, axis = 0))

json_obj = json.dumps(quality, indent = 4)
with open(args.output, "w") as f:
    f.write(json_obj)
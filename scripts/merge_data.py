#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 12:14:26 2020

@author: lasse
"""


import json
import yaml
import pandas as pd
import argparse
import os
import re
import sqlite3
import time
from datetime import date


def arg():
    parser = argparse.ArgumentParser("Script to collect Data, write to custom Multiqc table and insert into sqlite database")
    parser.add_argument("-c", "--baseconfig", help = "path to config file (.yaml)")
    parser.add_argument("-m", "--multiqc_config", help = "path to multiqc config file (.yaml)")
    return parser.parse_args()

def main():
    args = arg()
    with open(args.baseconfig, "r") as f:
        config_dict = yaml.load(f, Loader=yaml.FullLoader)
    column_names_lst = ["time", "date", "paired", "filename", "failed_filter", "Q30",
                        "gc_content", "mean_length","rel_Ns", "total_reads",
                        "fold_coverage", "insert_size", "duplication", 
                        "read1_mean","read1_A", "read1_T","read1_G","read1_C",
                        "read2_mean","read2_A", "read2_T","read2_G","read2_C"]
    fastp_result_path = config_dict["output"] + "/fastp_results/json/"
    time_current = time.time()
    date_current = date.today().strftime("%d/%m/%Y")
    
    filter_criteria_lst = column_names_lst[4:]
    operator_lst, fltr_threshold_lst, range_lst = get_operator_threshold_and_range_lst(config_dict, filter_criteria_lst)
    fltr_threshold_lst = [float(threshold) if threshold != None else "NaN" for threshold in fltr_threshold_lst]
    db_table = pd.DataFrame(columns = column_names_lst)
    taxIDs = ["taxID=" + taxID for taxID in config_dict["Filter"]["taxonomy_id"]["tax_ids"]]
    mqc_table = pd.DataFrame(columns = (column_names_lst[3:] + taxIDs))
    
    
    for filename in os.listdir(fastp_result_path):
        db_row_values = get_row_values(filename, time_current, date_current, config_dict)
        kraken_values_lst, kraken_names_lst = get_kraken_value(filename, config_dict["Filter"]["taxonomy_id"]["tax_ids"], config_dict)
        mqc_row_values = db_row_values[3:] + kraken_values_lst
        failed_filter_count = get_failed_filter_count(mqc_row_values[2:], operator_lst[1:], fltr_threshold_lst[1:], range_lst[1:])
        db_row_values[4] = failed_filter_count
        mqc_row_values[1] = failed_filter_count
        db_table.loc[len(db_table)] = db_row_values
        mqc_table.loc[len(mqc_table)] = mqc_row_values
    
    kraken_names_dict = dict(zip(taxIDs, kraken_names_lst))
    mqc_table = mqc_table.set_index("filename")
    mqc_table.index = [os.path.splitext(filename)[0] for filename in mqc_table.index.tolist()] #removes .fastq from filename
    column_ids = mqc_table.columns.tolist()
    column_titles, column_descriptions = get_column_titles_and_descriptions(column_ids, operator_lst, fltr_threshold_lst, config_dict)
    mqc_config_json = get_mqc_config_dict(column_ids, args.multiqc_config, operator_lst, fltr_threshold_lst, config_dict)
    mqc_table_json = get_mqc_table_dict(mqc_table, column_titles, column_descriptions, kraken_names_dict, config_dict)
    write_to_db(db_table, config_dict)
    
    with open(config_dict["output"] + "/mqc_config_edited.yaml", "w") as f:
        yaml.dump(mqc_config_json, f)
    
    with open(config_dict["output"] + "/custom_table_mqc.json", "w") as f:
        f.write(mqc_table_json)
        
    remove_files(config_dict)



def get_row_values(filename, time, date, config):
    file_basename = os.path.splitext(os.path.basename(filename))[0]
    failed_filter = 0
    fastp_result_dict = read_fastp_result(config["output"] + "/fastp_results/json/" + filename)
    fqstat_result_dict = read_fqstat_result(config["output"] + "/fqstat_results/" + file_basename + ".txt")
    genome_size = str(config["genome_size"])
    path_fastp_result_known_good = config["output"] + "/known_good_quality.json"
    paired = config["paired"]
    q30_rate = fastp_result_dict["summary"]["before_filtering"]["q30_rate"]
    gc_content = fastp_result_dict["summary"]["before_filtering"]["gc_content"]
    duplication_rate = fastp_result_dict["duplication"]["rate"]
    read1_mean = get_quality_difference("read1", "mean", fastp_result_dict, path_fastp_result_known_good)
    read1_A = get_quality_difference("read1", "A", fastp_result_dict, path_fastp_result_known_good)
    read1_T = get_quality_difference("read1", "T", fastp_result_dict, path_fastp_result_known_good)
    read1_G = get_quality_difference("read1", "G", fastp_result_dict, path_fastp_result_known_good)
    read1_C = get_quality_difference("read1", "C", fastp_result_dict, path_fastp_result_known_good)
    if config["paired"]:
        read2_mean = get_quality_difference("read2", "mean", fastp_result_dict, path_fastp_result_known_good)
        read2_A = get_quality_difference("read2", "A", fastp_result_dict, path_fastp_result_known_good)
        read2_T = get_quality_difference("read2", "T", fastp_result_dict, path_fastp_result_known_good)
        read2_G = get_quality_difference("read2", "G", fastp_result_dict, path_fastp_result_known_good)
        read2_C = get_quality_difference("read2", "C", fastp_result_dict, path_fastp_result_known_good)
        mean_length = ((fastp_result_dict["summary"]["before_filtering"]["read1_mean_length"] + fastp_result_dict["summary"]["before_filtering"]["read2_mean_length"])/2)
        rel_Ns = fqstat_result_dict["%Ns"][0]
        total_reads = fqstat_result_dict["total_reads"][0]
        fold_coverage = fqstat_result_dict["fold_coverage__target_size_" + genome_size + "Mb"][0]
        insert_size = get_insert_size(config["output"] + "/insert_size/" + file_basename + ".txt")
    else:
        read2_mean = "NaN"
        read2_A = "NaN"
        read2_T = "NaN"
        read2_G = "NaN"
        read2_C = "NaN"
        mean_length = (fastp_result_dict["summary"]["before_filtering"]["read1_mean_length"])
        rel_Ns = "NaN"
        total_reads = "NaN"
        fold_coverage = "NaN"
        insert_size = "NaN"
    return[time, date, paired, file_basename, failed_filter, q30_rate, gc_content, mean_length, rel_Ns, total_reads, fold_coverage, insert_size, duplication_rate, read1_mean, read1_A, read1_T, read1_G, read1_C, read2_mean, read2_A, read2_T, read2_G, read2_C]

def read_fastp_result(path):
    with open(path, "r") as f:
        fastp_result_json = json.load(f)
    return(fastp_result_json)

def read_fqstat_result(path):
    df = pd.read_csv(path, sep = "\t")
    fqstat_result_dict = df.to_dict(orient = "list")
    return(fqstat_result_dict)

def get_insert_size(path):
    df_insert_size = pd.read_csv(path, sep = ",", header=None)#
    insert_size = float(df_insert_size[0][1].split()[-1])
    return(insert_size)

def get_quality_difference(read, base, fastp_result_dict, path_known_good):
    data_known_good = read_fastp_result(path_known_good)[read][base]
    data_to_analyse = fastp_result_dict[read + "_before_filtering"]["quality_curves"][base]
    sum_difference = 0
    for i in range(len(data_known_good)):
        sum_difference+= abs(data_known_good[i]-data_to_analyse[i])
    return(sum_difference/len(data_known_good))

def get_operator_threshold_and_range_lst(config, filter_criteria_lst):
    operator_lst = []
    threshold_lst = []
    range_lst = []
    for fltr in filter_criteria_lst:
        if config["Filter"][fltr]["Operator"] and config["Filter"][fltr]["Value"]:
            operator = config["Filter"][fltr]["Operator"]
            operator_lst.append(operator)
            threshold_lst.append(config["Filter"][fltr]["Value"])
            if operator == "+-":
                range_lst.append(config["Filter"][fltr]["Range"])
            else:
                range_lst.append(None)
        else:
            operator_lst.append(None)
            threshold_lst.append(None)
    operator_lst.extend(config["Filter"]["taxonomy_id"]["Operator"])
    threshold_lst.extend(config["Filter"]["taxonomy_id"]["Values"])
    return(operator_lst, threshold_lst, range_lst)

def get_kraken_value(filename, taxIDs, config):
    file_basename = os.path.splitext(os.path.basename(filename))[0]
    path = config["output"] + "/kraken2_results/" + file_basename + ".kreport2"
    kraken_value_lst = []
    kraken_name_lst = []
    with open (path, "r") as f:
        df = pd.read_csv(f, sep = "\t", header = None)
    for taxID in taxIDs:
        try:
            idx = df[df[4]==int(taxID)].index.item()
            kraken_value_lst.append(df[0][idx])
            kraken_name_lst.append(df[5][idx])
        except ValueError:
            kraken_value_lst.append(0)
            kraken_name_lst.append("unknown_name")
    return(kraken_value_lst, kraken_name_lst)
    
def get_failed_filter_count(value_lst, operator_lst, threshold_lst, range_lst):
    counter = 0
    value_lst = [float(value) if value != None else None for value in value_lst]
    range_lst = [float(value) if value != None else None for value in range_lst]
    for i in range(len(value_lst)):
        if operator_lst[i] and threshold_lst[i] != None:
            if value_lst[i] != None:
                if operator_lst[i] == ">" and value_lst[i] <= threshold_lst[i]:
                    counter += 1
                elif operator_lst[i] == "<" and value_lst[i] >= threshold_lst[i]:
                    counter += 1
                elif operator_lst[i] == "=" and value_lst[i] != threshold_lst[i]:
                    counter += 1
                elif operator_lst[i] == ">=" and value_lst[i] < threshold_lst[i]:
                    counter += 1
                elif operator_lst[i] == "<=" and value_lst[i] > threshold_lst[i]:
                    counter += 1
                elif operator_lst[i] == "+-" and (value_lst[i] > threshold_lst[i] + range_lst[i] or value_lst[i] < threshold_lst[i] - range_lst[i]):
                    counter += 1
    return(counter)

def get_column_titles_and_descriptions(column_ids, operator_lst, thresholds_lst, config):
    column_title_dict = {
        "failed_filter": "Failed Filter",
        "Q30": "Q30 rate",
        "gc_content": "GC content",
        "mean_length": "Mean Readlength",
        "duplication": "Duplication rate",
        "read1_mean": "read1_mean",
        "read1_A": "read1_A",
        "read1_T": "read1_T",
        "read1_G": "read1_G",
        "read1_C": "read1_C",
        "read2_mean": "read2_mean",
        "read2_A": "read2_A",
        "read2_T": "read2_T",
        "read2_G": "read2_G",
        "read2_C": "read2_C",
        "rel_Ns": "%Ns",
        "total_reads": "Reads total",
        "fold_coverage": "Est. Fold Coverage",
        "insert_size": "Insert Size"
        }
    column_descriptions = []
    column_titles = []
    for i, colname in enumerate(column_ids):
        if colname in column_title_dict and (operator_lst[i] != None and thresholds_lst[i] != None):
            column_titles.append(column_title_dict[colname])
            if operator_lst[i] == "+-":
                column_descriptions.append("{} = {} +- {}".format(column_title_dict[colname], thresholds_lst[i], config["Filter"][colname]["Range"]))
            else:
                column_descriptions.append("{} {} {}".format(column_title_dict[colname], operator_lst[i], thresholds_lst[i]))
        elif colname in column_title_dict and (operator_lst[i] == None or thresholds_lst[i] == None):
            column_titles.append(column_title_dict[colname])
            column_descriptions.append(column_title_dict[colname])
        elif colname not in column_title_dict and (operator_lst[i] != None and thresholds_lst[i] != None):
            column_titles.append(colname)
            if operator_lst[i] == "+-":
                column_descriptions.append("{} = {} +- {}".format(colname, thresholds_lst[i], config["Filter"][colname]["Range"]))
            else:
                column_descriptions.append("{} {} {}".format(colname, operator_lst[i], thresholds_lst[i]))
        else:
            column_titles.append(colname)
            column_descriptions.append(colname)

    return(column_titles, column_descriptions)

def get_mqc_config_dict(column_ids, mqc_config_path, operator_lst, threshold_lst, config):
    with open(mqc_config_path, "r") as f:
        mqc_config_dict = yaml.load(f, Loader=yaml.FullLoader)
    mqc_config_dict["table_cond_formatting_rules"] = {}
    for i, colname in enumerate(column_ids):
        id_str = colname.replace(" ", "_").replace("=", "_")

        if operator_lst[i] != None and threshold_lst[i] != None:
            if operator_lst[i] == ">":
                pass_condition = [{"gt":threshold_lst[i]}]
                fail_condition = [{"lt":threshold_lst[i]},{"eq":threshold_lst[i]}]
            elif operator_lst[i] == "<":
                pass_condition = [{"lt":threshold_lst[i]}]
                fail_condition = [{"gt":threshold_lst[i]},{"eq":threshold_lst[i]}]
            elif operator_lst[i] == "=":
                pass_condition = [{"eq":threshold_lst[i]}]
                fail_condition = [{"gt":threshold_lst[i]},{"lt":threshold_lst[i]}]
            elif operator_lst[i] == ">=":
                pass_condition = [{"gt":threshold_lst[i]},{"eq":threshold_lst[i]}]
                fail_condition = [{"lt":threshold_lst[i]}]
            elif operator_lst[i] == "<=":
                pass_condition = [{"lt":threshold_lst[i]},{"eq":threshold_lst[i]}]
                fail_condition = [{"gt":threshold_lst[i]}]
            elif operator_lst[i] == "+-":
                range_value = float(config["Filter"][colname]["Range"])
                pass_condition = [{"gt":threshold_lst[i] - range_value}, {"lt":threshold_lst[i] + range_value}]
                fail_condition = [{"gt":threshold_lst[i] + range_value}, {"lt":threshold_lst[i] - range_value}]
            mqc_config_dict["table_cond_formatting_rules"][id_str] = {"pass":pass_condition, "fail":fail_condition}
    if not config["paired"]:
        mqc_config_dict["extra_fn_clean_exts"] = [mqc_config_dict["extra_fn_clean_exts"][0]] #keeps the program from removing "_R1" in signle end files
    return(mqc_config_dict)

def get_mqc_table_dict(mqc_table, column_titles, column_descriptions, kraken_names_dict, config):
    custom_table_dict = {
        "id": "filter_table",
        "section_name": "Filter Table",
        "description":
        """
        This table shows the values for each sample and criteria.
        Comparison to known good data is hidden by default for the specific bases.
        If the "Failed Filter" Column shows >1 but you can't see which filter failed, try to enable the columns through the "Configure Columns" button.
        """,
        "plot_type": "table",
        "pconfig":{
            "id": "filter_table",
            "title": "JSON table with ordered columns",
            },
        }
        
    custom_table_dict["headers"] = {}
    for i , col_id in enumerate(mqc_table.columns.tolist()):
        hide_col = False
        if col_id in config["hide_by_default"]:
            hide_col = True
        if not config["paired"] and col_id in ["read2_mean", "read2_A", "read2_T", "read2_C", "read2_G", "insert_size"]:
            hide_col = True
        col_config = {"hidden": hide_col,
                      "title": column_titles[i],
                      "description": column_descriptions[i],
                      "scale": False}
        if col_id in ["Q30", "gc_content", "duplication", "rel_Ns"]:
            col_config["format"] = "{:,.3f}"
        elif col_id in ["failed_filter", "total_reads"]:
            col_config["format"] = "{:,.0f}"
        elif col_id in ["fold_coverage"]:
            col_config["format"] = "{:,.2f}"
        elif re.match("taxID=(" + "|".join(config["Filter"]["taxonomy_id"]["tax_ids"]) + ")", col_id):
            col_config["format"] = "{:,.2f}"
            col_config["description"] = kraken_names_dict[col_id]
        custom_table_dict["headers"][col_id] = col_config
    custom_table_dict["data"] = mqc_table.to_dict("index")
    custom_table_json = json.dumps(custom_table_dict, indent = 4)
    return(custom_table_json)

def write_to_db(db_table, config):
    if config["sqlite_db"]:
        conn = sqlite3.connect(config["sqlite_db"])
        kraken_file_lst = []
        # mash_file_lst = []
        for filename in db_table["filename"].tolist():
            kraken_path = config["output"] + "/kraken2_results/" + filename + ".kreport2"
            # mash_path = config["output"] + "/mash_results/" + filename + ".tab"
            with open (kraken_path, "r") as kraken_file:
                kraken_file_lst.append(kraken_file.read())
            # with open (mash_path, "r") as mash_file:
            #     mash_file_lst.append(mash_file.read())

        db_table["kraken_file"] = kraken_file_lst
        # db_table["mash_file"] = mash_file_lst
        db_table.to_sql("qc_data", conn, if_exists="append", index=False)

def remove_files(config):
    output_dir = config["output"]
    if not config["keep_results"]["cutadapt"]:
        os.system("rm -r " + output_dir + "/cutadapt_results")
    if not config["keep_results"]["fastp"]:
        os.system("rm -r " + output_dir + "/fastp_results")
    if not config["keep_results"]["kraken2"]:
        os.system("rm -r " + output_dir + "/kraken2_results")
    if not config["keep_results"]["spades"]:
        os.system("rm -r " + output_dir + "/spades_results")
    if not config["keep_results"]["bowtie2"]:
        os.system("rm -r " + output_dir + "/bowtie2_results")
    if not config["keep_results"]["get_insert_size"]:
        os.system("rm -r " + output_dir + "/insert_size_results")
    if not config["keep_results"]["fqstat"]:
        os.system("rm -r " + output_dir + "/fqstat_results")
    if not config["keep_results"]["cutadapt_known_good"]:
        os.system("rm -r " + output_dir + "/cutadapt_results_known_good")
    if not config["keep_results"]["fastp_known_good"]:
        os.system("rm -r " + output_dir + "/fastp_results_known_good")
    if not config["keep_results"]["average_quality_score"]:
        os.system("rm " + output_dir + "/known_good_quality.json")
    if not config["keep_results"]["mqc_config"]:
        os.system("rm " + output_dir + "/mqc_config_edited.yaml")
    if not config["keep_results"]["mqc_table"]:
        os.system("rm " + output_dir + "/custom_table_mqc.json")

if __name__ == "__main__":
 	main()
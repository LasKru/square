import argparse
import json
import pdfkit
import markdown


def arg():
	parser = argparse.ArgumentParser("Create PDF Report from fastp, kraken and mash")
	parser.add_argument("-f", "--fastp", help = "path of json report from fastp", type = str)
	parser.add_argument("-k", "--kraken2", help = "path of krona plot of kraken2 result in html", type = str)
	parser.add_argument("-m", "--mash", help = "path of krona plot of mash result in html", type = str)
	parser.add_argument("-o", "--output", help = "optional output directory", type = str, default = "results")
	return parser.parse_args()

args = arg()

def read_json(path):
	with open(path, "r") as f:
		data_fastp = json.load(f)
	return (data_fastp)

data_fp = read_json(args.fastp)

mean_length = data_fp["summary"]["before_filtering"]["read1_mean_length"]
insert_size_peak = data_fp["insert_size"]["peak"]
total_reads = data_fp["summary"]["before_filtering"]["total_reads"]
total_bases = data_fp["summary"]["before_filtering"]["total_bases"]
gc_content = data_fp["summary"]["before_filtering"]["gc_content"]
x_values = []

for i in range((data_fp["read1_before_filtering"]["total_cycles"])):
    x_values.append(i)
A_values = data_fp["read1_before_filtering"]["quality_curves"]["A"]
T_values = data_fp["read1_before_filtering"]["quality_curves"]["T"]
C_values = data_fp["read1_before_filtering"]["quality_curves"]["C"]
G_values = data_fp["read1_before_filtering"]["quality_curves"]["G"]
mean_values = data_fp["read1_before_filtering"]["quality_curves"]["mean"]

style = """
<head>
    <script src="http://opengene.org/plotly-1.2.0.min.js"></script>
</head>

<style>
table, th, td {
    border:1px solid #999999;
    padding:2x;
    border-collapse:collapse;
    width:500px}
.section_title {color:#ffffff;
                font-size:20px;
                padding:5px;
                text-align:left;
                background:#0f18d8;
                margin-top:10px;
                margin-bottom:10px;
                font-weight:bold;}
.col1 {width:240px; font-weight:bold;}
</style>
"""

text = f"""
# QC Pipeline Report

<div class='section_div'>
<div class='section_title'><a name='summary'>Summary</a></div>

<table class='summary_table'>
<tr><td class='col1'>mean read length:</td><td class='col2'>{mean_length}</td></tr>
<tr><td class='col1'>insert size peak:</td><td class='col2'>{insert_size_peak}</td></tr>
<tr><td class='col1'>total reads:</td><td class='col2'>{total_reads}</td></tr>
<tr><td class='col1'>total bases:</td><td class='col2'>{total_bases}</td></tr>
<tr><td class='col1'>GC content:</td><td class='col2'>{gc_content}</td></tr>
</table>


"""
#x values, A values, x values, T values, x values, C values, G values, x values, MEAN
plot = """
<div class='section_div'>
<div class='section_title'><a name='read_quality'>Read Quality</a></div>
<div style="display:grid;grid-template-columns:repeat(2,1fr)">
<div><div class="figure" id="read_quality"></div>
<script>
			var data = [
				{
					x: %s,
					y: %s,
					name: "A",
					mode: "lines",
					line: { color: "rgba(128,128,0,1.0)", width: 1 },
				},
				{
					x: %s,
					y: %s,
					name: "T",
					mode: "lines",
					line: { color: "rgba(128,0,128,1.0)", width: 1 },
				},
				{
					x: %s,
					y: %s,
					name: "C",
					mode: "lines",
					line: { color: "rgba(0,255,0,1.0)", width: 1 },
				},
				{
					x: %s,
					y: %s,
					name: "G",
					mode: "lines",
					line: { color: "rgba(0,0,255,1.0)", width: 1 },
				},
				{
					x: %s ,
					y: %s ,
					name: "mean",
					mode: "lines",
					line: { color: "rgba(20,20,20,1.0)", width: 1 },
				},
			];
			var layout = { title: "", xaxis: { title: "position", type: "log" }, yaxis: { title: "quality" } };
			Plotly.newPlot("read_quality", data, layout);
		</script></div>
</div>
""" % (x_values,A_values,x_values,T_values,x_values,C_values,x_values,G_values,x_values,mean_values)



full_string = style+text+plot
html_string = markdown.markdown(full_string)

with open ("testfile.html", "w") as file:
    file.write(html_string)

pdfkit.from_file("testfile.html", "testfile.pdf")

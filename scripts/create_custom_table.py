#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 17 22:32:03 2020

@author: lasse
"""

import json
import yaml
import pandas as pd
import numpy as np
import argparse
import os
import re
import sqlite3

def arg():
    parser = argparse.ArgumentParser("Checking Filters")
    parser.add_argument("-d", "--directory", help = "Directory with JSON files to read")
    parser.add_argument("-c", "--config", help = "path to config file (.yaml)")
    parser.add_argument("-q", "--quality", help = "known good quality (.json)")
    parser.add_argument("-m", "--multiqc_config", help = "path to multiqc config file (.yaml)")
    return parser.parse_args()

with open(arg().config, "r") as f:
    config_dict = yaml.load(f, Loader=yaml.FullLoader)

if config_dict["paired"] == True:
    filter_lst = ["Q30", "gc_content", "mean_length","rel_Ns", "total_reads",
                  "fold_coverage", "insert_size", "duplication","read1_mean","read1_A",
                  "read1_T","read1_G","read1_C","read2_mean","read2_A",
                  "read2_T","read2_G","read2_C"]
else:
    filter_lst = ["Q30", "gc_content", "mean_length","rel_Ns", "total_reads",
                  "fold_coverage", "insert_size", "duplication","read1_mean","read1_A",
                  "read1_T","read1_G","read1_C"]


filter_exist_lst = []
operator_lst = []
value_lst = []

column_name_with_thresholds = ["Failed Filter"]

col_title_dict = {"overall": "Overall",
                  "Q30": "Q30 rate",
                  "gc_content": "GC content",
                  "mean_length": "Mean Readlength",
                  "duplication": "Duplication rate",
                  "read1_mean": "read1_mean",
                  "read1_A": "read1_A",
                  "read1_T": "read1_T",
                  "read1_G": "read1_T",
                  "read1_C": "read1_T",
                  "read2_mean": "read2_mean",
                  "read2_A": "read2_A",
                  "read2_T": "read2_T",
                  "read2_G": "read2_T",
                  "read2_C": "read2_T",
                  "rel_Ns": "%Ns",
                  "total_reads": "Reads total",
                  "fold_coverage": "Estimated Fold Coverage",
                  "insert_size": "Insert Size"}

for fltr in filter_lst:
    if config_dict[fltr] != None:
        filter_exist_lst.append(fltr)
        operator_lst.append(config_dict[fltr][0])
        value_lst.append(config_dict[fltr][1])
        column_name_with_thresholds.append(col_title_dict[fltr] + " " + config_dict[fltr][0] + " " + config_dict[fltr][1])

taxID_lst = []
for i in range(0, len(config_dict["taxonomy_ID"]), 3):
    taxID_lst.append(config_dict["taxonomy_ID"][i:i + 3])
taxID_df = pd.DataFrame(taxID_lst)


filter_exist_lst.extend(("taxID=" + taxID_df[0]).tolist())
operator_lst.extend(taxID_df[1].tolist())
value_lst.extend(taxID_df[2].tolist())
for row in range(len(taxID_df)):
    column_name_with_thresholds.append("{} {} {}".format(taxID_df.iloc[row][0], taxID_df.iloc[row][1], taxID_df.iloc[row][2]))


def read_json(path):
    with open(path, "r") as f:
        data_fastp = json.load(f)
    return (data_fastp)

def greater(value, threshold):
    if value > float(threshold):
        return("Pass")
    else:
        return("Fail")

def less(value, threshold):
    if value < float(threshold):
        return("Pass")
    else:
        return("Fail")

def equal(value, threshold):
    if value == float(threshold):
        return("Pass")
    else:
        return("Fail")

def greater_equal(value, threshold):
    if value >= float(threshold):
        return("Pass")
    else:
        return("Fail")

def less_equal(value, threshold):
    if value <= float(threshold):
        return("Pass")
    else:
        return("Fail")


def get_json_data(data, fltr):
    filter_to_json_dict = {
    "Q30": "q30_rate",
    "gc_content": "gc_content",
    "read1_mean": "mean",
    "read1_A": "A",
    "read1_T": "T",
    "read1_G": "G",
    "read1_C": "C",
    "read2_mean": "mean",
    "read2_A": "A",
    "read2_T": "T",
    "read2_G": "G",
    "read2_C": "C",
    }

    if fltr in ["Q30", "gc_content"]:
        return(data["summary"]["before_filtering"][filter_to_json_dict[fltr]])
    elif fltr == "mean_length":
        return((data["summary"]["before_filtering"]["read1_mean_length"] + data["summary"]["before_filtering"]["read2_mean_length"])/2)
    elif fltr == "duplication":
        return(data["duplication"]["rate"])
    elif fltr in ["read1_mean", "read1_A", "read1_T", "read1_G", "read1_C"]:
        data_known_good = read_json(arg().quality)["read1"][filter_to_json_dict[fltr]]
        data_control = data["read1_before_filtering"]["quality_curves"][filter_to_json_dict[fltr]]
        sum_difference = 0
        for i in range(len(data_known_good)):
            sum_difference += abs(data_known_good[i]-data_control[i])
        return(sum_difference/len(data_known_good))
    elif fltr in ["read2_mean", "read2_A", "read2_T", "read2_G", "read2_C"]:
        data_known_good = read_json(arg().quality)["read2"][filter_to_json_dict[fltr]]
        data_control = data["read2_before_filtering"]["quality_curves"][filter_to_json_dict[fltr]]
        sum_difference = 0
        for i in range(len(data_known_good)):
            sum_difference += abs(data_known_good[i]-data_control[i])
        return(sum_difference/len(data_known_good))

def read_fqstat_result(path):
    df = pd.read_csv(path, sep = "\t")
    fqstat_dict = df.to_dict(orient = "list")
    return(fqstat_dict)

def get_fqstat_data(data, fltr):
    filter_to_fqstat_dict = {
        "rel_Ns": "%Ns",
        "total_reads": "total_reads"}
    filter_to_fqstat_dict["fold_coverage"] = "fold_coverage__target_size_" + str(config_dict["genome_size"]) + "Mb"
    return(data[filter_to_fqstat_dict[fltr]])[0]

def get_insert_size(path):
    df_insert_size = pd.read_csv(path, sep = ",", header=None)#
    insert_size = float(df_insert_size[0][1].split()[-1])
    return(insert_size)

def get_kraken_value(path, taxID):
    with open (path, "r") as f:
        df = pd.read_csv(f, sep  = "\t", header = None)
        try:
            idx = df[df[4]==int(taxID)].index.item()
            value = df[0][idx]
            name = df[5][idx]
        except ValueError:
            value = 0
            name = "unknown name"
    return (value, name)


def check_operator(operator, value, threshold):
    if operator == ">":
        return(greater(value, threshold))
    elif operator == "<":
        return(less(value,threshold))
    elif operator == "=":
        return(equal(value, threshold))
    elif operator == ">=":
        return(greater_equal(value, threshold))
    elif operator == "<=":
        return(less_equal(value, threshold))
    else:
        exit("unknown operator: " + operator)


files_basename = []

values_df = []

for filename in os.listdir(arg().directory):
    file_basename = os.path.splitext(os.path.basename(filename))[0]
    files_basename.append(file_basename)
    row_values_lst = [0]
    fastp_data = read_json(arg().directory + "/" + filename)
    fqstat_data = read_fqstat_result(config_dict["output"] + "/fqstat_results/" + file_basename + ".txt")
    kraken_name_dict = {}
    for i in range(len(filter_exist_lst)):
        if filter_exist_lst[i] in ["Q30","gc_content","mean_length","duplication","read1_mean","read1_A","read1_T","read1_G","read1_C","read2_mean","read2_A","read2_T","read2_G","read2_C"]:
            value = get_json_data(fastp_data, filter_exist_lst[i])
        elif filter_exist_lst[i] in ["rel_Ns", "total_reads", "fold_coverage"]:
            value = get_fqstat_data(fqstat_data, filter_exist_lst[i])
        elif filter_exist_lst[i] == "insert_size":
            value = get_insert_size(config_dict["output"] + "/insert_size/" + file_basename + ".txt")
        elif filter_exist_lst[i] in taxID_df[0].tolist():
            value, tax_name = get_kraken_value(config_dict["output"] + "/kraken2_results/" + file_basename + ".kreport2", filter_exist_lst[i])
            kraken_name_dict[filter_exist_lst[i]] = tax_name
        row_values_lst.append(value)
        checked_filter_str = check_operator(operator_lst[i], value, value_lst[i])
        if checked_filter_str == "Fail":
            row_values_lst[0] += 1
    values_df.append(row_values_lst)


column_names_values = []
for col in filter_exist_lst:
    column_names_values.append("values_" + col)


with open(arg().multiqc_config, "r") as f:
    mqc_config_dict = yaml.load(f, Loader=yaml.FullLoader)

mqc_config_dict["table_cond_formatting_rules"] = {"values_failed_filter":
                                                  {"pass": [{"eq": 0},],
                                                  "fail": [{"gt": 0},]}}


for i in range(len(column_names_values)):
    id_str = column_names_values[i].replace(" ", "_")
    if operator_lst[i] == ">":
        pass_condition = [{"gt":float(value_lst[i])}]
        fail_condition = [{"lt":float(value_lst[i])},{"eq":float(value_lst[i])}]
    elif operator_lst[i] == "<":
        pass_condition = [{"lt":float(value_lst[i])}]
        fail_condition = [{"gt":float(value_lst[i])},{"eq":float(value_lst[i])}]
    elif operator_lst[i] == "=":
        pass_condition = [{"eq":float(value_lst[i])}]
        fail_condition = [{"gt":float(value_lst[i])},{"lt":float(value_lst[i])}]
    elif operator_lst[i] == ">=":
        pass_condition = [{"gt":float(value_lst[i])},{"eq":float(value_lst[i])}]
        fail_condition = [{"lt":float(value_lst[i])}]
    elif operator_lst[i] == "<=":
        pass_condition = [{"lt":float(value_lst[i])},{"eq":float(value_lst[i])}]
        fail_condition = [{"gt":float(value_lst[i])}]

    mqc_config_dict["table_cond_formatting_rules"][id_str] = {"pass":pass_condition, "fail":fail_condition}


def set_column_config(custom_table_dict, colid_lst, colnames_lst):
    custom_table_dict["headers"] = {}
    for i in range(len(colid_lst)):
        hide_col = False
        if re.match('values_read(1|2)_[ATGC]', colid_lst[i]):
            hide_col = True

        col_config = {"hidden": hide_col,
                      "title": colnames_lst[i]}

        if re.match('values_(Q30|gc_content|duplication|rel_Ns)', colid_lst[i]):
            col_config["format"] = "{:,.3f}"
        elif colid_lst[i] in ['values_failed_filter','values_total_reads']:
            col_config["format"] = "{:,.0f}"
        elif colid_lst[i] == 'values_fold_coverage':
            col_config["format"] = "{:,.2f}"
        elif re.match('values_('+ '|'.join(taxID_df[0].tolist()) + ')', colid_lst[i]):
            col_config["format"] = "{:,.2f}"
            col_config["description"] = kraken_name_dict[colid_lst[i].lstrip("values_")]
        custom_table_dict["headers"][colid_lst[i]] = col_config
    return(custom_table_dict)


filter_values_dict = {
        "id": "filter_values_table",
        "section_name": "Filter Values",
        "description":
        """
        This table shows the values for each sample and criteria.
        Comparison to known good data is hidden by default for the specific bases.
        If the "Failed Filter" Column shows >1 but you can't see which filter failed, try to enable the columns through the "Configure Columns" button.
        """,
        "plot_type": "table",
        "pconfig":{
            "id": "filter_values_table",
            "title": "JSON table with ordered columns",
            },
        }

    
column_names_values = ["values_failed_filter"] + column_names_values


filter_values_dict = set_column_config(filter_values_dict, column_names_values, column_name_with_thresholds)

df_values_table = pd.DataFrame(values_df,
                                columns = column_names_values,
                                index = files_basename)

filter_values_dict["data"] = df_values_table.to_dict("index")

json_obj_values_table = json.dumps(filter_values_dict, indent = 4)


# print(["Failed Filter"] + filter_exist_lst)
# print(values_df)
# filenames = [os.path.splitext(name)[0] for name in files_basename]
# print(filenames)

# write_to_db(filter_exist_lst, values_df, files_basename)

def write_to_db(filter_exist_lst, values_df, files_basename):
    columns = ["Filename", "Failed Filter"] + filter_exist_lst
    filenames = pd.DataFrame([os.path.splitext(name)[0] for name in files_basename])
    values_df = pd.DataFrame(values_df)
    rows = pd.concat([filenames, values_df], axis = 1)
    if config_dict["db"]!= None:
        if not os.path.isfile(config_dict["db"]):
            conn = sqlite3.connect("/home/lasse/Desktop/Masterarbeit/qc-pipeline/results/test_db.db")
            c = conn.cursor()
            command = "CREATE TABLE IF NOT EXISTS data ([id] INTEGER PRIMARY KEY, [Filename] text"
            for colname in columns[1:]:
                command += ", [{}] real".format(colname)
            command += ")"
            c.execute(command)
        else:
            conn = sqlite3.connect("/home/lasse/Desktop/Masterarbeit/qc-pipeline/results/test_db.db")
            c = conn.cursor()
        # insert_cmd = "INSERT INTO data VALUES (%s)" %
        
        conn.commit()
    
write_to_db(filter_exist_lst, values_df, files_basename)

with open(config_dict["output"] + "/filter_values_mqc.json", "w") as f:
    f.write(json_obj_values_table)
with open(config_dict["output"] + "/mqc_config_edited.yaml", "w") as f:
    yaml.dump(mqc_config_dict, f)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  1 18:54:20 2020

@author: lasse
"""

import argparse
import os
import yaml
from sys import exit

dir_path = os.path.dirname(os.path.realpath(__file__))

def arg():
    parser = argparse.ArgumentParser("SQUARE - Sequencing Quality Assurance Report")
    parser.add_argument("-c", "--config", help = "path to config file [.yaml]", type = str)
    return parser.parse_args()
    
def check_file(file): #check if files exist
	if not os.path.isfile(file):
		exit("error: file '" + file + "' not found")

def check_dir(directory): #check if directory exists
	if not os.path.isdir(directory):
		exit("error: directory '" + directory + "' not found")

def main():
    args = arg()
    with open (args.config) as f:
        config_data= yaml.safe_load(f)
    for key in ["input_1", "input_2", "mash_db", "ref_genome"]:
        if config_data[key]:
            check_file(config_data[key])
    for key in ["directory", "known_good_reads", "kraken2_db"]:
        if config_data[key]:
            check_dir(config_data[key])
    if type(config_data["paired"]) != bool:
        exit("error: read type not defined (single/paired)")
    
    with open("config/snakemake_config.yaml", "w") as f:
        yaml.dump(config_data, f)
    
    os.system("snakemake --use-conda --cores " + str(config_data["cores"]))



if __name__ == "__main__":
	main()

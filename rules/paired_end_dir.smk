rule cutadapt:
	input:
		input_1 = config["directory"] + "/{name}_R1_{suffix}.gz",
		input_2 = config["directory"] + "/{name}_R2_{suffix}.gz"
	output:
		output_1 = config["output"] + "/cutadapt_results/{name}_R1_{suffix}.gz",
		output_2 = config["output"] + "/cutadapt_results/{name}_R2_{suffix}.gz"
	params:
		adapter_1 = config["adapter"],
		adapter_2 = config["adapter_2"]
	conda:
		os.path.join(workflow.basedir, "envs/cutadapt-env.yaml")
	message:
		"Executing adaptertrimming for {input.input_1} and {input.input_2}"
	shell:
		"cutadapt -a {params.adapter_1} -A {params.adapter_2} -o {output.output_1} -p {output.output_2} {input.input_1} {input.input_2}"

rule fastp:
	input:
		input_1 = config["output"] + "/cutadapt_results/{name}_R1_{suffix}.gz",
		input_2 = config["output"] + "/cutadapt_results/{name}_R2_{suffix}.gz"
	output:
		html_report = config["output"] + "/fastp_results/html/{name}_{suffix}.html",
		json_report = config["output"] + "/fastp_results/json/{name}_{suffix}.json"
	conda:
		os.path.join(workflow.basedir, "envs/fastp-env.yaml")
	message:
		"Executing fastp for {input.input_1} and {input.input_2}"
	shell:
		"fastp -i {input.input_1} -I {input.input_2} -h {output.html_report} -j {output.json_report} -Q -L -A -G "

rule kraken2:
	input:
		input_1 = config["output"] + "/cutadapt_results/{name}_R1_{suffix}.gz",
		input_2 = config["output"] + "/cutadapt_results/{name}_R2_{suffix}.gz",
		database = config["kraken2_db"]
	output:
		config["output"] + "/kraken2_results/{name}_{suffix}.kreport2"
	conda:
		os.path.join(workflow.basedir,"envs/kraken2-env.yaml")
	message:
		"Executing kraken2 for {input.input_1} and {input.input_2}"
	log:
		config["output"] + "/kraken2_results/logs/{name}_{suffix}.log"
	shell:
		"kraken2 --db {input.database} --report {output} --paired {input.input_1} {input.input_2} --memory-mapping > {log}"

rule concatenate_paired_ends:
	input:
		input_1 = config["output"] + "/cutadapt_results/{name}_R1_{suffix}.gz",
		input_2 = config["output"] + "/cutadapt_results/{name}_R2_{suffix}.gz"
	output:
		output_1 = temp(config["output"] + "/mash_results/{name}_{suffix}.gz")
	shell:
		"cat {input.input_1} {input.input_2} > {output.output_1}"

rule mash:
	input:
		input_1 = config["output"] + "/mash_results/{name}_{suffix}.gz",
		database = config["mash_db"]
	output:
		output = config["output"] + "/mash_results/{name}_{suffix}.tab"
	conda:
		os.path.join(workflow.basedir,"envs/mash-env.yaml")
	message:
		"Executing mash for {input.input_1}"
	shell:
		"mash screen {input.database} {input.input_1}| sort -gr > {output.output}"

rule spades:
	input:
		fw_reads = config["output"] + "/cutadapt_results/{name}_R1_{suffix}.gz",
		rv_reads = config["output"] + "/cutadapt_results/{name}_R2_{suffix}.gz",
	output:
		contigs = config["output"] + "/spades_results/{name}_{suffix}/contigs.fasta"
	params:
		output_dir = config["output"] + "/spades_results/{name}_{suffix}/"
	conda:
		os.path.join(workflow.basedir, "envs/spades-env.yaml")
	log:
		config["output"] + "/spades_results/logs/{name}_{suffix}.log"
	shell:
		"spades.py --careful -1 {input.fw_reads} -2 {input.rv_reads} -o {params.output_dir} >{log}"

rule create_index:
	input:
		ref_genome = get_ref_genome()
	output:
		out_1 = get_index_output() + ".1.bt2",
		out_rev_1 = get_index_output() + ".rev.1.bt2"
	conda:
		os.path.join(workflow.basedir,"envs/bowtie2-env.yaml")
	params:
		basename = get_index_output()
	message:
		"Indexing reference genome for bowtie2"
	shell:
		"bowtie2-build {input.ref_genome} {params.basename}"

rule bowtie2:
	input:
		in_1 = get_index_output() + ".1.bt2",
		in_rev_1 = get_index_output() + ".rev.1.bt2",
		input_1 = config["output"] + "/cutadapt_results/{name}_R1_{suffix}.gz",
		input_2 = config["output"] + "/cutadapt_results/{name}_R2_{suffix}.gz"
	output:
		config["output"] + "/bowtie2_results/{name}_{suffix}.sam"
	conda:
		os.path.join(workflow.basedir,"envs/bowtie2-env.yaml")
	params:
		index = get_index_output()
	message:
		"Mapping with Bowtie2 for {input.input_1} and {input.input_2}"
	log:
		config["output"] + "/bowtie2_results/{name}_{suffix}.log"
	shell:
		"bowtie2 -x {params.index} -1 {input.input_1} -2 {input.input_2} -S {output} 2> {log}"

rule get_insert_size:
	input:
		samfile = config["output"] + "/bowtie2_results/{name}_{suffix}.sam"
	output:
		insert_size = config["output"] + "/insert_size/{name}_{suffix}.txt"
	shell:
		"python3 scripts/getinsertsize.py {input.samfile} > {output.insert_size}"

rule fqstat:
	input:
		input_1 = config["output"] + "/cutadapt_results/{name}_R1_{suffix}.gz",
		input_2 = config["output"] + "/cutadapt_results/{name}_R2_{suffix}.gz"
	output:
		out = config["output"] + "/fqstat_results/{name}_{suffix}.txt"
	params:
		genome_size = get_command_for_fqstat()
	message:
		"Running fqstat for {input.input_1} and {input.input_2}"
	shell:
		"scripts/fqstat.py {input.input_1} {input.input_2} {params.genome_size} -o {output.out}"

rule cutadapt_known_good:
	input:
		input_1 = config["known_good_reads"] + "/{name}_R1_{suffix}.gz",
		input_2 = config["known_good_reads"] + "/{name}_R2_{suffix}.gz"
	output:
		output_1 = config["output"] + "/cutadapt_results_known_good/{name}_R1_{suffix}.gz",
		output_2 = config["output"] + "/cutadapt_results_known_good/{name}_R2_{suffix}.gz"
	params:
		adapter_1 = config["adapter"],
		adapter_2 = config["adapter_2"]
	conda:
		os.path.join(workflow.basedir, "envs/cutadapt-env.yaml")
	message:
		"Executing adaptertrimming for {input.input_1} and {input.input_2}"
	shell:
		"cutadapt -a {params.adapter_1} -A {params.adapter_2} -o {output.output_1} -p {output.output_2} {input.input_1} {input.input_2}"

rule fastp_known_good:
	input:
		input_1 = config["output"]+"/cutadapt_results_known_good/{name}_R1_{suffix}.gz",
		input_2 = config["output"]+"/cutadapt_results_known_good/{name}_R2_{suffix}.gz"
	output:
		json_report = config["output"] + "/fastp_results_known_good/json/{name}_{suffix}.json",
		html_report = config["output"] + "/fastp_results/html/{name}_{suffix}.html",
	conda:
		os.path.join(workflow.basedir, "envs/fastp-env.yaml")
	message:
		"Executing fastp for {input.input_1} and {input.input_2}"
	shell:
		"fastp -i {input.input_1} -I {input.input_2} -j {output.json_report} -h {output.html_report} -Q -L -A -G "

rule average_quality_score:
	input:
		fastp_results = expand(config["output"] + "/fastp_results_known_good/json/{name}_{suffix}.json", zip, name = name_known_good.name, suffix = name_known_good.suffix),
	output:
		average_quality = config["output"] + "/known_good_quality.json"
	params:
		fastp_results_dir = config["output"] + "/fastp_results_known_good/json"
	message:
		"Calculating average quality score from known good reads"
	shell:
		"scripts/calc_average_quality.py -d {params.fastp_results_dir} -o {output.average_quality} -p"

rule merge_data:
	input:
		fastp_results = expand(config["output"] + "/fastp_results/json/{name}_{suffix}.json", zip, name = fw_files.name, suffix = fw_files.suffix),
		average_quality = config["output"] + "/known_good_quality.json",
		insert_size = expand(config["output"] + "/insert_size/{name}_{suffix}.txt", zip, name = fw_files.name, suffix = fw_files.suffix),
		fqstat_results = expand(config["output"] + "/fqstat_results/{name}_{suffix}.txt", zip, name = fw_files.name, suffix = fw_files.suffix),
		kraken2_results = expand(config["output"] + "/kraken2_results/{name}_{suffix}.kreport2", zip, name = fw_files.name, suffix = fw_files.suffix),
		mash_results = get_mash_command_for_mqc(),
	output:
		custom_table = config["output"] + "/custom_table_mqc.json",
		edited_mqc_config = config["output"] + "/mqc_config_edited.yaml"
	params:
		config_file = os.path.join(workflow.basedir, "config/snakemake_config.yaml"),
		mqc_config_file = os.path.join(workflow.basedir, "config/multiqc_config.yaml")
	message:
		"Creating custom tables for multiqc report"
	shell:
		"scripts/merge_data.py -c {params.config_file} -m {params.mqc_config_file}"

rule multiqc:
	input:
		custom_mqc_table = config["output"] + "/custom_table_mqc.json",
		custom_mqc_config = config["output"] + "/mqc_config_edited.yaml"
	output:
		config["output"] + "/multiqc_report/multiqc_report.html"
	conda:
		os.path.join(workflow.basedir,"envs/multiqc-env.yaml")
	params:
		fastp_dir = config["output"] + "/fastp_results/json/",
		kraken2_dir = config["output"] + "/kraken2_results/",
		bowtie_dir = config["output"] + "/bowtie2_results/",
		output_dir = config["output"] + "/multiqc_report/",
	message:
		"Creating multiqc-report"
	shell:
		"multiqc {params.fastp_dir} {params.kraken2_dir} {params.bowtie_dir} {input.custom_mqc_table} -o {params.output_dir} --config {input.custom_mqc_config}"

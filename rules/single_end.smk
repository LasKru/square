rule cutadapt:
	input:
		input_1 = config["input_1"]
	output:
		config["output"] + "/cutadapt_results/" + os.path.basename(config["input_1"])
	params:
		adapter = config["adapter"]
	conda:
		os.path.join(workflow.basedir, "envs/cutadapt-env.yaml")
	message:
		"executing adaptertrimming for " + os.path.basename(config["input_1"])
	shell:
		"cutadapt -a {params.adapter} -o {output} {input.input_1}"

rule fastp:
	input:
		input_1 = config["output"] + "/cutadapt_results/" + os.path.basename(config["input_1"])
	output:
		html_report = config["output"] + "/fastp_results/html/" + filename + ".html",
		json_report = config["output"] + "/fastp_results/json/" + os.path.splitext(os.path.basename(config["input_1"]))[0] + ".json"
	conda:
		os.path.join(workflow.basedir, "envs/fastp-env.yaml")
	message:
		"Executing fastp for {input.input_1}"
	shell:
		"fastp -i {input.input_1} -h {output.html_report} -j {output.json_report} -Q -L -A -G "

rule kraken2:
	input:
		input_1 = config["output"] + "/cutadapt_results/" + os.path.basename(config["input_1"]),
		database = config["kraken2_db"]
	output:
		config["output"]+"/kraken2_results/"+ os.path.splitext(os.path.basename(config["input_1"]))[0] + ".kreport2"
	conda:
		os.path.join(workflow.basedir,"envs/kraken2-env.yaml")
	message:
		"Executing kraken2 for {input.input_1}"
	log:
		config["output"] + "/kraken2_results/logs/" + os.path.basename(config["input_1"]) + ".log"
	shell:
		"kraken2 --db {input.database} --report {output} {input.input_1} --memory-mapping > {log}"

rule download_minikraken:
	output:
		temp("db/minikraken_8GB_202003.tgz")
	shell:
		"wget -P db ftp://ftp.ccb.jhu.edu/pub/data/kraken2_dbs/minikraken_8GB_202003.tgz"

rule unzip_minikraken:
	input:
		"db/minikraken_8GB_202003.tgz"
	output:
		"db/minikraken_8GB_20200312/"
	shell:
		"tar -xvzf {input} -C db"

rule mash:
	input:
		input_1 = config["output"] + "/cutadapt_results/" + os.path.basename(config["input_1"]),
		database = config["mash_db"]
	output:
		output = config["output"] + "/mash_results/"+ os.path.splitext(os.path.basename(config["input_1"]))[0] + ".tab"
	conda:
		os.path.join(workflow.basedir,"envs/mash-env.yaml")
	message:
		"Executing mash for {input.input_1}"
	shell:
		"mash screen {input.database} {input.input_1} | sort -gr > {output.output}"

rule download_refseq:
	output:
		"db/refseq.genomes.k21s1000.msh"
	shell:
		"wget -P db https://gembox.cbcb.umd.edu/mash/refseq.genomes.k21s1000.msh"

rule multiqc: #ADD BOWTIE SUPPORT
	input:
		fastp_results = config["output"] + "/fastp_results/json/" + filename + ".json",
		kraken2_results = config["output"] + "/kraken2_results/"+ filename + ".kreport2",
		mash_results = config["output"] + "/mash_results/"+ filename + ".tab",
		custom_column = config["output"] + "/genstats_mqc.json",
		custom_table = config["output"] + "/custom_table_mqc.json"
	output:
		config["output"] + "/multiqc_report/multiqc_report.html"
	conda:
		os.path.join(workflow.basedir,"envs/multiqc-env.yaml")
	params:
		fastp_dir = config["output"] + "/fastp_results/json/",
		kraken2_dir = config["output"] + "/kraken2_results/",
		output_dir = config["output"] + "/multiqc_report/",
		config_path = os.path.join(workflow.basedir,"config/multiqc_config.yaml")
	message:
		"Creating multiqc-report"
	shell:
		"multiqc {params.fastp_dir} {params.kraken2_dir} {input.custom_column} {input.custom_table} -o {params.output_dir} --config {params.config_path}"

rule cutadapt_known_good:
	input:
		input_1 = config["known_good_reads"] + "/{name}.gz",
	output:
		output_1 = config["output"] + "/cutadapt_results_known_good/{name}.gz"
	params:
		adapter_1 = config["adapter"]
	conda:
		os.path.join(workflow.basedir, "envs/cutadapt-env.yaml")
	message:
		"Executing adaptertrimming for {input.input_1}"
	shell:
		"cutadapt -a {params.adapter_1} -o {output.output_1} {input.input_1}"


rule fastp_known_good:
	input:
		input_1 = config["output"]+"/cutadapt_results_known_good/{name}.gz"
	output:
		json_report = config["output"] + "/fastp_results_known_good/json/{name}.json"
	conda:
		os.path.join(workflow.basedir, "envs/fastp-env.yaml")
	message:
		"Executing fastp for {input.input_1}"
	shell:
		"fastp -i {input.input_1} -j {output.json_report} -Q -L -A -G "


rule average_quality_score:
	input:
		fastp_results = expand(config["output"] + "/fastp_results_known_good/json/{name}.json", name = name_known_good.name)
	output:
		average_quality = config["output"] + "/known_good_quality.json"
	params:
		fastp_results_dir = config["output"] + "/fastp_results_known_good/json"
	message:
		"Calculating average quality score from known good reads"
	shell:
		"scripts/calc_average_quality.py -d {params.fastp_results_dir} -o {output.average_quality}"

rule create_custom_table:
	input:
		fastp_result = config["output"] + "/fastp_results/json/" + os.path.splitext(os.path.basename(config["input_1"]))[0] + ".json",
		average_quality = config["output"] + "/known_good_quality.json"
	output:
		custom_column = config["output"] + "/genstats_mqc.json",
		custom_table = config["output"] + "/custom_table_mqc.json"
	params:
		fastp_results_dir = config["output"] + "/fastp_results/json",
		config_file = os.path.join(workflow.basedir, "config/snakemake_config.yaml")
	message:
		"Creating custom tables for multiqc report"
	shell:
		"scripts/create_custom_table.py -d {params.fastp_results_dir} -c {params.config_file} -q {input.average_quality}"

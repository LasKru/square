rule cutadapt:
	input:
		input_1 = config["directory"] + "/{name}.gz"
	output:
		config["output"] + "/cutadapt_results/{name}.gz"
	params:
		adapter = config["adapter"]
	conda:
		os.path.join(workflow.basedir, "envs/cutadapt-env.yaml")
	message:
		"executing adaptertrimming for {input.input_1}"
	shell:
		"cutadapt -a {params.adapter} -o {output} {input.input_1}"

rule fastp:
	input:
		config["output"] + "/cutadapt_results/{name}.gz"
	output:
		html_report = config["output"] + "/fastp_results/html/{name}.html",
		json_report = config["output"] + "/fastp_results/json/{name}.json"
	conda:
		os.path.join(workflow.basedir, "envs/fastp-env.yaml")
	message:
		"Executing fastp for {input}"
	shell:
		"fastp -i {input} -h {output.html_report} -j {output.json_report} -Q -L -A -G "

rule kraken2:
	input:
		input_1 = config["output"] + "/cutadapt_results/{name}.gz",
		database = config["kraken2_db"]
	output:
		config["output"]+"/kraken2_results/{name}.kreport2"
	conda:
		os.path.join(workflow.basedir,"envs/kraken2-env.yaml")
	message:
		"Executing kraken2 for {input.input_1}"
	log:
		config["output"] + "/kraken2_results/logs/{name}.log"
	shell:
		"kraken2 --db {input.database} --report {output} {input.input_1} --memory-mapping > {log}"

rule mash:
	input:
		input_1 = config["output"] + "/cutadapt_results/{name}.gz",
		database = config["mash_db"]
	output:
		output = config["output"] + "/mash_results/{name}.tab"
	conda:
		os.path.join(workflow.basedir,"envs/mash-env.yaml")
	message:
		"Executing mash for {input.input_1}"
	shell:
		"mash screen {input.database} {input.input_1} | sort -gr > {output.output}"

rule spades:
	input:
		input_1 = config["output"] + "/cutadapt_results/{name}.gz",
	output:
		contigs = config["output"] + "/spades_results/{name}/contigs.fasta"
	params:
		output_dir = config["output"] + "/spades_results/{name}/"
	conda:
		os.path.join(workflow.basedir, "envs/spades-env.yaml")
	shell:
		"spades.py --careful -s {input.input_1} -o {params.output_dir}"

rule create_index:
	input:
		ref_genome = get_ref_genome()
	output:
		out_1 = get_index_output() + ".1.bt2",
		out_rev_1 = get_index_output() + ".rev.1.bt2"
	conda:
		os.path.join(workflow.basedir,"envs/bowtie2-env.yaml")
	params:
		basename = get_index_output()
	message:
		"Indexing reference genome for bowtie2"
	shell:
		"bowtie2-build {input.ref_genome} {params.basename}"

rule bowtie2:
	input:
		in_1 = get_index_output() + ".1.bt2",
		in_rev_1 = get_index_output() + ".rev.1.bt2",
		input_1 = config["output"] + "/cutadapt_results/{name}.gz",
	output:
		config["output"] + "/bowtie2_results/{name}.sam"
	conda:
		os.path.join(workflow.basedir,"envs/bowtie2-env.yaml")
	params:
		index = get_index_output()
	message:
		"Mapping with Bowtie2 for {input.input_1}"
	log:
		config["output"] + "/bowtie2_results/{name}.log"
	shell:
		"bowtie2 -x {params.index} -U {input.input_1} -S {output} 2> {log}"

rule fqstat:
	input:
		input_1 = config["output"] + "/cutadapt_results/{name}.gz",
	output:
		out = config["output"] + "/fqstat_results/{name}.txt"
	params:
		genome_size = get_command_for_fqstat()
	message:
		"Running fqstat for {input.input_1}"
	shell:
		"scripts/fqstat.py --single {input.input_1} {params.genome_size} -o {output.out}"

rule cutadapt_known_good:
	input:
		input_1 = config["known_good_reads"] + "/{name}.gz",
	output:
		output_1 = config["output"] + "/cutadapt_results_known_good/{name}.gz"
	params:
		adapter_1 = config["adapter"]
	conda:
		os.path.join(workflow.basedir, "envs/cutadapt-env.yaml")
	message:
		"Executing adaptertrimming for {input.input_1}"
	shell:
		"cutadapt -a {params.adapter_1} -o {output.output_1} {input.input_1}"

rule fastp_known_good:
	input:
		input_1 = config["output"] + "/cutadapt_results_known_good/{name}.gz"
	output:
		json_report = config["output"] + "/fastp_results_known_good/json/{name}.json",
		html_report = config["output"] + "/fastp_results_known_good/html/{name}.html"
	conda:
		os.path.join(workflow.basedir, "envs/fastp-env.yaml")
	message:
		"Executing fastp for {input.input_1}"
	shell:
		"fastp -i {input.input_1} -j {output.json_report} -h {output.html_report} -Q -L -A -G "

rule average_quality_score:
	input:
		fastp_results = expand(config["output"] + "/fastp_results_known_good/json/{name}.json", name = name_known_good.name)
	output:
		average_quality = config["output"] + "/known_good_quality.json"
	params:
		fastp_results_dir = config["output"] + "/fastp_results_known_good/json"
	message:
		"Calculating average quality score from known good reads"
	shell:
		"scripts/calc_average_quality.py -d {params.fastp_results_dir} -o {output.average_quality}"

rule merge_data:
	input:
		fastp_results = expand(config["output"] + "/fastp_results/json/{name}.json", name = files.name),
		average_quality = config["output"] + "/known_good_quality.json",
		fqstat_results = expand(config["output"] + "/fqstat_results/{name}.txt", name = files.name),
		kraken2_results = expand(config["output"] + "/kraken2_results/{name}.kreport2", name = files.name),
		mash_results = get_mash_command_for_mqc(),
	output:
		custom_table = config["output"] + "/custom_table_mqc.json",
		edited_mqc_config = config["output"] + "/mqc_config_edited.yaml"
	params:
		config_file = os.path.join(workflow.basedir, "config/snakemake_config.yaml"),
		mqc_config_file = os.path.join(workflow.basedir, "config/multiqc_config.yaml")
	message:
		"Creating custom tables for multiqc report"
	shell:
		"scripts/merge_data.py -c {params.config_file} -m {params.mqc_config_file}"

rule multiqc:
	input:
		bowtie2_results = expand(config["output"] + "/bowtie2_results/{name}.sam", name = files.name),
		custom_mqc_table = config["output"] + "/custom_table_mqc.json",
		custom_mqc_config = config["output"] + "/mqc_config_edited.yaml"
	output:
		config["output"] + "/multiqc_report/multiqc_report.html"
	conda:
		os.path.join(workflow.basedir,"envs/multiqc-env.yaml")
	params:
		fastp_dir = config["output"] + "/fastp_results/json/",
		kraken2_dir = config["output"] + "/kraken2_results/",
		bowtie_dir = config["output"] + "/bowtie2_results/",
		output_dir = config["output"] + "/multiqc_report/",
	message:
		"Creating multiqc-report"
	shell:
		"multiqc {params.fastp_dir} {params.kraken2_dir} {params.bowtie_dir} {input.custom_mqc_table} -o {params.output_dir} --config {input.custom_mqc_config}"

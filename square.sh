#!/bin/bash
eval "$(conda shell.bash hook)"
conda env create -n square --file /home/lasse/Desktop/Masterarbeit/qc-pipeline/envs/pipeline-env.yaml
conda activate square
echo "activated conda environment"
python3 /home/lasse/Desktop/Masterarbeit/qc-pipeline/scripts/wrapper.py -c $1
echo "Done"

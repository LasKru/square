# SQUARE

## Description
SQUARE ("Sequencing Quality Assurance REport") is a quality control pipeline for DNA sequencing using tools like cutadapt, fastp, kraken2, mash, bowtie2, spades and multiqc.

## Installation


## Run
You need:
- file(s) of paired or single end reads [seq_ID_(1|2)_suffix.fastq.gz // .fq.gz]
- kraken2 database (can be downloaded <a href="http://ccb.jhu.edu/software/kraken2/index.shtml?t=downloads" title="kraken_db_link"> here</a>)
- (optional) mash database (can be downloaded <a href="https://mash.readthedocs.io/en/latest/data.html" title="mash_db_link"> here</a>)
- known good single or paired end reads from the same sequencer [seq_ID_(1|2)_suffix.fastq.gz // .fq.gz]
- (recommended) reference genome of the suspected species. If not specified, Spades will run an assembly and bowtie will align the reads to the assembled genome
- config file [.yaml] with all required informations

The command is really simple, because all needed information is declared in the config file.
```bash
python3 square.py -c config_file.yaml
```
### Config File Structure
The config file is a simple .yaml file containing all required information.
#### Reads
You can run the pipeline either with all files in a single directory or with single specific files. If you want to run the pipeline with a single file with single end reads, insert the path to the file at the "input_1" key. If you want to run the pipeline with paired end reads, insert the path to the forward reads at the "input_1" and the path to the reverse reads at the "input_2" key. If you want to analyse all files in a directory, you can keep "input_1" and "input_2" empty and insert the path to the directory at the "directory" key.

```yaml
input_1:
input_2:
directory: /home/lasse/Documents/QC_data/Ecoli/good/HS
```
If you have paired end reads, set the "paired" key to "true". If you have single end reads, set it to "false".
```yaml
paired: false
```

#### Output
You can set the directory where the results will be saved at the "output" key.
```yaml
output: /home/lasse/Desktop/Masterarbeit/qc-pipeline/results
```
#### SQLite Database
You can set the path to the sqlite database at the "sqlite_db" key. If left empty, it will not save your results to a database. If the specified file does not exist, the program will create a new sqlite database with the specified name.
```yaml
sqlite_db: /home/lasse/Desktop/Masterarbeit/qc-pipeline/results/test_db.db
```
#### Adapter sequences
You can set the adapter sequence to be trimmed through cutadapt at the "adapter" and "adapter_2" key. If you have paired end reads, "adapter" will be trimmed from the forward reads and "adapter_2" will be trimmed from the reverse reads. If you have single end reads, "adapter_2" will be ignored. You need to research the adaptersequences used in your kit.
```yaml
adapter: AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
adapter_2: AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
```

#### Known good Reads
You can set the path to the directory with known good sequence data at the "known_good_reads" key. These will be used to compare the quality scores of the analysed reads with the quality scores of the known good reads.
```yaml
known_good_reads: /home/lasse/Desktop/Masterarbeit/qc-pipeline/test_data
```

#### Databases
You can set the path to the kraken2 and mash databases at the "kraken2_db" and "mash_db" keys. "kraken2_db" requires the path to a directory, while "mash_db" requires a .msh file. As of right now, the mash results are not shown in the multiqc report and are not saved in the sqlite database. If you want to manually view them, set the "mash" key at "keep_results" to "true". If set to "false", you dont need to set a mash database, because mash will not be run.
```yaml
kraken2_db: /home/lasse/Desktop/Masterarbeit/qc-pipeline/db/minikraken_8GB_20200312
mash_db: /home/lasse/Desktop/Masterarbeit/qc-pipeline/db/refseq.genomes.k21s1000.msh
```

#### Genome
NEEDS TO BE DONE
```yaml
genome_size: 2.8
ref_genome:
```
#### Cores
You can set the amount of cores used for the pipeline at the "cores" key.
```yaml
cores: 1
```

#### Multiqc
The analysed data will be visualised in a multiqc report [.html]. The second table in the report shows each filterable value. If you dont want some columns to be shown, you can specify these at the "hide_by_default" key. If you have defined filter criteria for these values, they will still be counted for the "failed filter" value.
```yaml
hide_by_default:
    - 'read1_A'
    - 'read1_T'
    - 'read1_G'
    - 'read1_C'
    - 'read2_A'
    - 'read2_T'
    - 'read2_G'
    - 'read2_C'
```

There are several definable filter criteria. You can set the Operator and the Value for each at the "Filter" key. In the following example you can see the structure of two possible filter criteria, "failed_filter" and "Q30".
```yaml
Filter:
    failed_filter:
        Operator: '='
        Value: '0'
    Q30:
        Operator: '>'
        Value: '0.5'
```
The "failed_filter" criterion counts every time one of the other filter has failed. In this example, the operator is "=" and the Value is "0". In this case, it will be shown as passed if the count of failed filter is exactly 0. Acceptable operators are "=", ">", "<", ">=", "<=".
In the following list you can see all potential filter criteria. If "Operator", "Value" or both are kept empty, the value will still be shown in the table but it will not be highlighted as "passed" or "failed" and it will not be counted at the "failed_filter" counter.
- failed_filter &#8594; count of values that did not pass the specified filter
- Q30 &#8594; rate of reads with a quality score > 30, value required as decimal
- gc_content &#8594; gc content, value required as decimal
- mean_length &#8594; mean length of the reads
- rel_Ns &#8594; TBD//relative amnount of undefined bases//TBD
- total_reads &#8594; total amount of reads
- fold_coverage &#8594; TBD
- insert_size &#8594; insert size, only for paired end reads
- duplication &#8594; duplication rate, value required as decimal
- read1_mean &#8594; difference of the quality score of all bases compared to known good data for the forward reads
- read1_A &#8594; difference of the quality score of arginine bases compared to known good data for the forward reads
- read1_T &#8594; difference of the quality score of thymine bases compared to known good data for the forward reads
- read1_G &#8594; difference of the quality score of guanine bases compared to known good data for the forward reads
- read1_C &#8594; difference of the quality score of cytosine bases compared to known good data for the forward reads
- read2_mean &#8594; difference of the quality score of all bases compared to known good data for the reverse reads (paired end reads only)
- read2_A &#8594; difference of the quality score of arginine bases compared to known good data for the reverse reads (paired end reads only)
- read2_T &#8594; difference of the quality score of thymine bases compared to known good data for the reverse reads (paired end reads only)
- read2_G &#8594; difference of the quality score of guanine bases compared to known good data for the reverse reads (paired end reads only)
- read2_C &#8594; difference of the quality score of cytosine bases compared to known good data for the reverse reads (paired end reads only)

You can also set a filter for the kraken2 results but the structure is a little bit different. You can set a filter for as many taxonomy IDs as you wish. Therefore, the taxonomyIDs, the operator and the values are stored in lists.

```yaml
Filter:
    taxonomy_id:
        tax_ids:
        - '0'
        - '1'
    Operator:
        - '<'
        - '>'
    Values:
        - '10'
        - '80'
```
In this example, one filter checks if the reads assigned to the taxonomy ID "0" are less than 10 %. The other filter checks if the reads assigned to the taxonomy ID "1" are at least 80%. This means that the reads should be at most 10% undefined and at least 80% assigned to "root", which means they should not be undefined. These lists can be expanded for as many taxonomy IDs you want.

#### Keep results
After finishing the pipeline, most files will be deleted and only the multiqc report and the database remain. If you wnat to keep some files to view them manually you can set them here. The mash results are not visualised in the multiqc report and are also not saved to the sqlite database. If the "mash" key is set to "false", mash will not be run at all to speed up the pipeline.
```yaml
cutadapt: false
fastp: false
kraken2: false
mash: false
spades: false
bowtie2: false
get_insert_size: false
fqstat: false
cutadapt_known_good: false
fastp_known_good: false
average_quality_score: true
mqc_config: true
mqc_table: true
```

#### Customising Reports
You can customise your MultiQC report via the "config/multiqc_config" file. Here you can add customisations like titles, introductory text, report time and report logo. How you can specify these information is explained in the <a href="https://multiqc.info/docs/#customising-reports" title="mqc_docs"> multiqc documentation</a>.


## How it works
```mermaid
graph TD

	seq_data(sequencing data) --> cutadapt;
	cutadapt --> bowtie2;
	cutadapt -.-> spades
	cutadapt --> fqstat;
	cutadapt --> fastp;
	cutadapt --> kraken2;
	spades -.-> bowtie2;
	ref_genome(reference genome) -.-> bowtie2;
	bowtie2 --> get_insert_size.py
	bowtie2 --> multiqc
	cutadapt -. optional .-> mash;
	average_quality_score.py --> merge_data.py
	fastp --> merge_data.py
	kraken2 --> merge_data.py
	get_insert_size.py --> merge_data.py
	fqstat --> merge_data.py
	merge_data.py --> multiqc
	fastp --> multiqc
	kraken2 --> multiqc
	merge_data.py --> sqlite_db
	subgraph "known good data"
	known_good_data --> cutadapt_kgd(cutadapt);
	cutadapt_kgd --> fastp_kgd(fastp);
	fastp_kgd --> average_quality_score.py
	end

```

### Step 1: Adaptertrimming with cutadapt
Removal of in config file specified adapter sequences.
Output:
- trimmed reads (fastq.gz // fq.gz)

### Step 2: fastp
Input:
- trimmed reads (fastq.gz // fq.gz)

Output:
- html and json reports with information about:
	- mean length
	- total reads
	- total bases
	- Q20 bases
	- Q30 bases
	- gc content
	- duplication rate
	- read quality

### Step 3: kraken2
Input:
- trimmed reads (fastq.gz // gq.gz)
- kraken2 database

Output:
- kraken2 report about taxonomic classification (fastq.kreport2 // fq.kreport2)

### Step 4: mash
Input:
- trimmed reads (fastq.gz // gq.gz)
- mash database

Output:
- mash report about taxonomic classification (fastq.tab // fq.tab)

### Step 5: fqstat
Input:
- trimmed reads (fastq.gz // gq.gz)

Output:
- .txt file with information about:
	- mean length
	- median length
	- total reads
	- total bases
	- gc content
	- %Ns
	- total Ns
	- fold coverage

### Step 6: Bowtie2

### Step 7: Get Insert Size

### Step 8: Known Good Average Quality Score

### Step 9: Merge Data

### Step 10: Multiqc Report

### Step 11: sqlite database

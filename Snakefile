import os
import json

configfile: "config/snakemake_config.yaml"

def get_command_for_fqstat():
	if config["genome_size"] == None:
		return("")
	else:
		return("--size " + str(config["genome_size"]))

def get_ref_genome():
	if config["ref_genome"] != None:
		return(config["ref_genome"])
	elif config["directory"] != None and config["paired"]:
		return(config["output"] + "/spades_results/{name}_{suffix}/contigs.fasta")
	elif config["directory"] != None and config["paired"] == False:
		return(config["output"] + "/spades_results/{name}/contigs.fasta")
	elif config["directory"] == None and config["paired"]:
		return(config["output"] + "/spades_results/" + outputname + "/contigs.fasta")

def get_index_output():
	if config["ref_genome"] != None:
		genome_file_name = os.path.splitext(os.path.basename(config["ref_genome"]))[0]
		return(config["output"] + "/bowtie2_results/ref_genome/" + genome_file_name + "/" + genome_file_name)
	elif config["directory"] != None and config["paired"]:
		return(config["output"] + "/bowtie2_results/ref_genome/{name}_{suffix}/{name}_{suffix}")
	elif config["directory"] != None and config["paired"] == False:
		return(config["output"] + "/bowtie2_results/ref_genome/{name}/{name}")
	elif config["directory"] == None and config["paired"]:
		return(config["output"] + "/bowtie2_results/ref_genome/" + outputname + "/" + outputname)

def get_mash_command_for_mqc():
	if not config["keep_results"]["mash"]:
		return([])
	elif config["directory"] != None and config["paired"]:
		return(expand(config["output"] + "/mash_results/{name}_{suffix}.tab", zip, name = fw_files.name, suffix = fw_files.suffix))
	elif config["directory"] != None and config["paired"] == False:
		return(expand(config["output"] + "/mash_results/{name}.tab", name = files.name))
	elif config["directory"] == None and config["paired"]:
		return(config["output"] + "/mash_results/" + outputname + ".tab")

if not config["paired"] and config["directory"] == None: #single end reads single file
	name_known_good = glob_wildcards(config["known_good_reads"] + "/{name}.gz")
	filename = os.path.splitext(os.path.basename(config["input_1"]))[0]
	rule all:
		input:
			config["output"] + "/multiqc_report/multiqc_report.html"
	include:
		"rules/single_end.smk"

elif config["paired"] and config["directory"] == None: #paired end reads single files
	name_known_good = glob_wildcards(config["known_good_reads"] + "/{name}_R1_{suffix}.gz")
	basename = os.path.basename(config["input_1"]) # mit Endung
	basename_2 = os.path.basename(config["input_2"])
	outputname = os.path.splitext(basename)[0].replace("_R1_", "_") # ohne Endung und ohne "_R1_"
	rule all:
		input:
			config["output"] + "/multiqc_report/multiqc_report.html"
	include:
		"rules/paired_end.smk"

elif not config["paired"] and config["directory"] != None: #single end reads directory
	name_known_good = glob_wildcards(config["known_good_reads"] + "/{name}.gz")
	files = glob_wildcards(config["directory"] + "/{name}.gz")
	rule all:
		input:
			config["output"] + "/multiqc_report/multiqc_report.html"
	include:
		"rules/single_end_dir.smk"

elif config["paired"] and config["directory"] != None: #paired end reads directory
	name_known_good = glob_wildcards(config["known_good_reads"] + "/{name}_R1_{suffix}.gz")
	fw_files = glob_wildcards(config["directory"] + "/{name}_R1_{suffix}.gz")
	rule all:
		input:
			config["output"] + "/multiqc_report/multiqc_report.html"
	include:
		"rules/paired_end_dir.smk"
